//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-02 16:43:44 korskov>

package tld.localdomain.localhost.Otus2019.L13;

final class LMutex {
    long val;
    LMutex() { val = 0; }
    public long get() { return val; }
    public void set(final long n) { val = n; }
    public String toString() { return Long.toString(val); }
}
