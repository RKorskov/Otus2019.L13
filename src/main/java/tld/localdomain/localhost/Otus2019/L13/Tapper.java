//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-02 16:47:25 korskov>

package tld.localdomain.localhost.Otus2019.L13;

import java.util.Random;

/*
 * L13: JMM
 * Два потока печатают числа от 1 до 10, потом от 10 до 1.
 * Надо сделать так, чтобы числа чередовались, т.е. получился такой вывод:
 * Поток 1:1 2 3 4 5 6 7 8 9 10 9 8 7 6 5 4 3 2 1 2 3 4...
 * Поток 2: 1 2 3 4 5 6 7 8 9 10 9 8 7 6 5 4 3 2 1 2 3...
 */

/**
 * реализация mutex'а на Long и synchronized methods
 */

public class Tapper extends Thread implements Runnable {

    private final static short LIMIT_LOW = 1, LIMIT_UP = 10;
    private static final int ITERS_MAX = Integer.MAX_VALUE;
    private static char[] 号字 //
        = {'\0', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十'};

    private final static Random RNG = new Random();
    private short counter;
    private boolean dir;
    private LMutex mutex;
    private int iters;

    // Tapper(short cnt_start, boolean cnt_dir) {;}
    Tapper(LMutex mutex) {
        counter = 1;
        dir = false;
        this.mutex = mutex;
        iters = 0;
    }

    public void run() {
        for(;iters < ITERS_MAX; randsleep()) {
            if(!turn_is_my())
                continue;
            if(!lock_is_free())
                continue;
            // https://docs.oracle.com/javase/tutorial/essential/concurrency/locksync.html
            if(!lock_get())
                continue;
            counter_print();
            counter_update();
            lock_release(); // always should be true!
            ++iters;
        }
    }

    private boolean turn_is_my() {
        return mutex.get() != -getId();
    }

    private boolean lock_is_free() {
        return mutex.get() < 0;
    }

    synchronized private boolean lock_get() {
        if(mutex.get() >= 0)
            return false;
        mutex.set(getId());
        return true;
    }

    synchronized private boolean lock_release() {
        if(mutex.get() != getId())
            return false;
        mutex.set(-getId());
        return true;
    }

    private void randsleep() {
        try {
            Thread.sleep(256 + RNG.nextInt(256));
        }
        catch (InterruptedException iex) {;}
    }

    private void counter_update() {
        if(counter == LIMIT_LOW || counter == LIMIT_UP)
            dir = !dir;
        if(dir)
            ++counter;
        else
            --counter;
    }

    private void counter_print() {
        if((getId() & 1) == 0)
            System.out.printf("%d ", counter);
        else
            System.out.printf("%c ", 号字[counter]);
    }
}
