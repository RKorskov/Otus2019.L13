//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-06 16:40:24 korskov>

package tld.localdomain.localhost.Otus2019.L13;

import java.util.concurrent.atomic.AtomicLong;

/**
 * L13: JMM
 * Два потока печатают числа от 1 до 10, потом от 10 до 1.
 * Надо сделать так, чтобы числа чередовались, т.е. получился такой вывод:
 * Поток 1:1 2 3 4 5 6 7 8 9 10 9 8 7 6 5 4 3 2 1 2 3 4...
 * Поток 2: 1 2 3 4 5 6 7 8 9 10 9 8 7 6 5 4 3 2 1 2 3...
 */

public class Main {
    public static void main(final String[] args) {
        if(args.length < 1)
            help();
        switch(args[0].toUpperCase()) {
        case "A":
        case "ATOM":
            eval_atom();
            break;
        case "S":
        case "SYNC":
            eval_sync();
            break;
        case "W":
        case "WAIT":
            eval_wait();
            break;
        default:
            help();
        }
    }

    private static void help() {
        System.out.println("Main (sync|atom)");
        System.out.println("mvn exec:java -Dexec.args=\"(sync|atom)\"");
        System.exit(1);
    }

    private static void eval_sync() {
        LMutex mutex = new LMutex();
        Tapper tap0 = new Tapper(mutex);
        Tapper tap1 = new Tapper(mutex);
        /*
        Thread tap0 = new Thread(new Tapper(mutex));
        Thread tap1 = new Thread(new Tapper(mutex));
        */
        mutex.set(-tap0.getId());
        System.out.println("start all");
        tap0.start();
        tap1.start();
    }

    private static void eval_atom() {
        AtomicLong mutex = new AtomicLong();
        AtomicTapper tap0 = new AtomicTapper(mutex);
        AtomicTapper tap1 = new AtomicTapper(mutex);
        mutex.set(-tap0.getId());
        System.out.println("start all");
        tap0.start();
        tap1.start();
    }

    private static void eval_wait() {
        LMutex mutex = new LMutex();
        WaitingTapper tap0 = new WaitingTapper(mutex);
        WaitingTapper tap1 = new WaitingTapper(mutex);
        mutex.set(-tap0.getId());
        System.out.println("start all");
        tap0.start();
        tap1.start();
    }
}
